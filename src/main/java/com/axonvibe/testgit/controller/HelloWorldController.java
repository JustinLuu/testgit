package com.axonvibe.testgit.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping()
public class HelloWorldController {
    @GetMapping("/hello")
    public String showHelloWorld() {
        System.out.println("Hello World!");
        return "Hello World!";
    }

}
